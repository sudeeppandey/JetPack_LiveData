# Project Overview

Model View ViewModel pattern features mainly LiveData and ViewModel classes. Persistence is implemented via Room. A RoomDatabase instance is a Singleton. Data Access Objects provide
ways to run CRUD operations on database. POJO classes represent data model. ViewModel class can create database and subscribe to database changes using LiveData object. LiveData
object holds the latest data. MainActivity which is a LifeCycleOwner then observes the LiveData object and displays latest data on its UI views. ViewModel objects survive 
configuration changes and are destroyed only when the LifeCycleOwner terminates completely. Hence, ViewModel persists data in memory.

### Structure

![alt text](structure.PNG "Wireframe 1")

### Robolectric Unit Testing

***DataAccessTest.java***, ***DateConverterTest.java*** and ***RoomDatabaseTest.java*** represent robolectric unit test classes. These test cases confirm that operations like creation of
database, accessing database and running CRUD, populating database -via main thread as well as background thread work fine.

### Instrumentation Testing

***LiveDataTestUtil.java***, ***MvvmTest.java***, ***UIEspressoTest.java*** represent instrumentation test classes. Key feature of these tests is to check if database access objects
populate livedata objects correctly. An Espresso test is also included to cover UI functional testing.