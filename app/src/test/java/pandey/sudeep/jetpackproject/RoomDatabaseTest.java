package pandey.sudeep.jetpackproject;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class RoomDatabaseTest {

    private Context context= RuntimeEnvironment.application;
    private AppDatabase db;

    @Before
    public void setup(){
        db = AppDatabase.getInMemoryDatabase(context);
    }

    @After
    public void teardown(){
        db.close();

    }

    @Test
    public void testAppDatabaseGetsInstantiatedCorrectly() {

        SupportSQLiteOpenHelper helper = db.getOpenHelper();
        SupportSQLiteDatabase database = helper.getWritableDatabase();
        assertTrue(database.isOpen());
        assertEquals(database.getVersion(), 1);
        ContentValues values = new ContentValues();
        values.put("id",5);
        values.put("name","Sudeep");
        values.put("age",33);
        ContentValues values2 = new ContentValues();
        values2.put("name","Dikshya");
        values2.put("age",26);
        values2.put("id",7);
        database.insert("User",5,values);
        database.insert("User",5,values2);
        Cursor cursor = database.query("Select name from User");
        assertEquals(cursor.getCount(),2);
        cursor.close();


    }

    @Test
    public void testRoomDatabaseGetsDestroyed(){

    }
}

