package pandey.sudeep.jetpackproject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Date;

import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class DateConverterTest {

    @Test
    public void testConverterWorksCorrectly(){

        assertTrue(DateConverter.toDate(123456L)instanceof Date);
        assertTrue(DateConverter.toTimestamp(new Date(1220227200L * 1000))instanceof Long);
    }
}
