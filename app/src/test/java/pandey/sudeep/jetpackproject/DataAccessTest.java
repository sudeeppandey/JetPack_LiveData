package pandey.sudeep.jetpackproject;

import android.content.Context;
import android.database.Cursor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class DataAccessTest {

    private Context context= RuntimeEnvironment.application;
    private AppDatabase roomDB;

    @Before
    public void setup(){
        roomDB = AppDatabase.getInMemoryDatabase(context);
    }

    @After
    public void teardown(){
        roomDB.close();
    }

    @Test
    public void populateSyncWorksCorrectly(){

        DatabaseInitializer.populateSync(roomDB);

        Cursor cursor = roomDB.query("Select * from User",null);
        assertEquals(cursor.getCount(),3);

        if (cursor.moveToFirst()) {
            String id = cursor.getString(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String lastname = cursor.getString(cursor.getColumnIndex("lastName"));
            int age = cursor.getInt(cursor.getColumnIndex("age"));

            assertEquals(id, "1");
            assertEquals(name, "Jason");
            assertEquals(lastname, "Seaver");
            assertEquals(age, 40);

            cursor.moveToNext();
            String id2 = cursor.getString(cursor.getColumnIndex("id"));
            String name2 = cursor.getString(cursor.getColumnIndex("name"));
            String lastname2 = cursor.getString(cursor.getColumnIndex("lastName"));
            int age2 = cursor.getInt(cursor.getColumnIndex("age"));

            assertEquals(id2, "2");
            assertEquals(name2, "Mike");
            assertEquals(lastname2, "Seaver");
            assertEquals(age2, 12);

            cursor.moveToNext();
            String id3 = cursor.getString(cursor.getColumnIndex("id"));
            String name3 = cursor.getString(cursor.getColumnIndex("name"));
            String lastname3 = cursor.getString(cursor.getColumnIndex("lastName"));
            int age3 = cursor.getInt(cursor.getColumnIndex("age"));

            assertEquals(id3, "3");
            assertEquals(name3, "Carol");
            assertEquals(lastname3, "Seaver");
            assertEquals(age3, 15);

            cursor.close();
        }

        Cursor book_cursor = roomDB.query("Select * from Book",null);
        if(book_cursor.moveToFirst()) {
            String id_1 = book_cursor.getString(book_cursor.getColumnIndex("id"));
            String title1 = book_cursor.getString(book_cursor.getColumnIndex("title"));
            assertEquals(id_1, "1");
            assertEquals(title1, "Dune");

            book_cursor.moveToNext();
            String id_2 = book_cursor.getString(book_cursor.getColumnIndex("id"));
            String title2 = book_cursor.getString(book_cursor.getColumnIndex("title"));
            assertEquals(id_2, "2");
            assertEquals(title2, "1984");

            book_cursor.moveToNext();
            String id_3 = book_cursor.getString(book_cursor.getColumnIndex("id"));
            String title3 = book_cursor.getString(book_cursor.getColumnIndex("title"));
            assertEquals(id_3, "3");
            assertEquals(title3, "The War of the Worlds");


            book_cursor.moveToNext();
            String id_4 = book_cursor.getString(book_cursor.getColumnIndex("id"));
            String title4 = book_cursor.getString(book_cursor.getColumnIndex("title"));
            assertEquals(id_4, "4");
            assertEquals(title4, "Brave New World");

            book_cursor.moveToNext();
            String id_5 = book_cursor.getString(book_cursor.getColumnIndex("id"));
            String title5 = book_cursor.getString(book_cursor.getColumnIndex("title"));
            assertEquals(id_5, "5");
            assertEquals(title5, "Foundation");

            book_cursor.close();
        }

        Cursor loan_cursor = roomDB.query("Select * from Loan",null);
        if(loan_cursor.moveToFirst()){

            String id_l1 = loan_cursor.getString(loan_cursor.getColumnIndex("id"));
            Long startTime_l1 = loan_cursor.getLong(loan_cursor.getColumnIndex("startTime"));
            Long endTime_l1 = loan_cursor.getLong(loan_cursor.getColumnIndex("endTime"));
            String userId_l1 = loan_cursor.getString(loan_cursor.getColumnIndex("user_id"));
            String bookId_l1 = loan_cursor.getString(loan_cursor.getColumnIndex("book_id"));

            assertEquals(id_l1,"1");
            assertTrue(startTime_l1 instanceof Long);
            assertTrue(endTime_l1 instanceof Long);
            assertEquals(userId_l1,"1");
            assertEquals(bookId_l1,"1");

            loan_cursor.moveToNext();
            String id_l2 = loan_cursor.getString(loan_cursor.getColumnIndex("id"));
            Long startTime_l2 = loan_cursor.getLong(loan_cursor.getColumnIndex("startTime"));
            Long endTime_l2 = loan_cursor.getLong(loan_cursor.getColumnIndex("endTime"));
            String userId_l2 = loan_cursor.getString(loan_cursor.getColumnIndex("user_id"));
            String bookId_l2 = loan_cursor.getString(loan_cursor.getColumnIndex("book_id"));

            assertEquals(id_l2,"2");
            assertTrue(startTime_l2 instanceof Long);
            assertTrue(endTime_l2 instanceof Long);
            assertEquals(userId_l2,"2");
            assertEquals(bookId_l2,"1");

            loan_cursor.moveToNext();
            String id_l3 = loan_cursor.getString(loan_cursor.getColumnIndex("id"));
            Long startTime_l3 = loan_cursor.getLong(loan_cursor.getColumnIndex("startTime"));
            Long endTime_l3 = loan_cursor.getLong(loan_cursor.getColumnIndex("endTime"));
            String userId_l3 = loan_cursor.getString(loan_cursor.getColumnIndex("user_id"));
            String bookId_l3 = loan_cursor.getString(loan_cursor.getColumnIndex("book_id"));

            assertEquals(id_l3,"3");
            assertTrue(startTime_l3 instanceof Long);
            assertTrue(endTime_l3 instanceof Long);
            assertEquals(userId_l3,"2");
            assertEquals(bookId_l3,"2");

            loan_cursor.moveToNext();
            String id_l4 = loan_cursor.getString(loan_cursor.getColumnIndex("id"));
            Long startTime_l4 = loan_cursor.getLong(loan_cursor.getColumnIndex("startTime"));
            Long endTime_l4 = loan_cursor.getLong(loan_cursor.getColumnIndex("endTime"));
            String userId_l4 = loan_cursor.getString(loan_cursor.getColumnIndex("user_id"));
            String bookId_l4 = loan_cursor.getString(loan_cursor.getColumnIndex("book_id"));

            assertEquals(id_l4,"4");
            assertTrue(startTime_l4 instanceof Long);
            assertTrue(endTime_l4 instanceof Long);
            assertEquals(userId_l4,"2");
            assertEquals(bookId_l4,"3");

            loan_cursor.moveToNext();
            String id_l5 = loan_cursor.getString(loan_cursor.getColumnIndex("id"));
            Long startTime_l5 = loan_cursor.getLong(loan_cursor.getColumnIndex("startTime"));
            Long endTime_l5 = loan_cursor.getLong(loan_cursor.getColumnIndex("endTime"));
            String userId_l5 = loan_cursor.getString(loan_cursor.getColumnIndex("user_id"));
            String bookId_l5 = loan_cursor.getString(loan_cursor.getColumnIndex("book_id"));

            assertEquals(id_l5,"5");
            assertTrue(startTime_l5 instanceof Long);
            assertTrue(endTime_l5 instanceof Long);
            assertEquals(userId_l5,"2");
            assertEquals(bookId_l5,"4");

            loan_cursor.close();

            //Now Test That Calling the method again won't create duplicates.
            DatabaseInitializer.populateSync(roomDB);

            Cursor c7 = roomDB.query("Select * from User",null);
            assertEquals(c7.getCount(),3);
            c7.close();
            Cursor c8 = roomDB.query("Select * from Book",null);
            assertEquals(c8.getCount(),5);
            c8.close();
            Cursor c9 = roomDB.query("Select * from Loan",null);
            assertEquals(c9.getCount(),5);
            c9.close();

            //Reset data
            roomDB.loanModel().deleteAll();
            roomDB.bookModel().deleteAll();
            roomDB.userModel().deleteAll();

            Cursor cursor10 = roomDB.query("Select * from User",null);
            assertEquals(cursor10.getCount(),0);
            cursor10.close();
            //Check the same tests above via AsyncTask

            DatabaseInitializer.populateAsync(roomDB);

            Cursor cursor_async = roomDB.query("Select * from User",null);
            assertEquals(cursor_async.getCount(),3);

            if (cursor_async.moveToFirst()) {
                String id = cursor_async.getString(cursor_async.getColumnIndex("id"));
                String name = cursor_async.getString(cursor_async.getColumnIndex("name"));
                String lastname = cursor_async.getString(cursor_async.getColumnIndex("lastName"));
                int age = cursor_async.getInt(cursor_async.getColumnIndex("age"));

                assertEquals(id, "1");
                assertEquals(name, "Jason");
                assertEquals(lastname, "Seaver");
                assertEquals(age, 40);

                cursor_async.moveToNext();
                String id2 = cursor_async.getString(cursor_async.getColumnIndex("id"));
                String name2 = cursor_async.getString(cursor_async.getColumnIndex("name"));
                String lastname2 = cursor_async.getString(cursor.getColumnIndex("lastName"));
                int age2 = cursor_async.getInt(cursor_async.getColumnIndex("age"));

                assertEquals(id2, "2");
                assertEquals(name2, "Mike");
                assertEquals(lastname2, "Seaver");
                assertEquals(age2, 12);

                cursor_async.moveToNext();
                String id3 = cursor_async.getString(cursor_async.getColumnIndex("id"));
                String name3 = cursor_async.getString(cursor_async.getColumnIndex("name"));
                String lastname3 = cursor_async.getString(cursor_async.getColumnIndex("lastName"));
                int age3 = cursor_async.getInt(cursor_async.getColumnIndex("age"));

                assertEquals(id3, "3");
                assertEquals(name3, "Carol");
                assertEquals(lastname3, "Seaver");
                assertEquals(age3, 15);

                cursor.close();
            }

            Cursor book_cursor_async = roomDB.query("Select * from Book",null);
            if(book_cursor_async.moveToFirst()) {
                String id_1 = book_cursor_async.getString(book_cursor_async.getColumnIndex("id"));
                String title1 = book_cursor_async.getString(book_cursor_async.getColumnIndex("title"));
                assertEquals(id_1, "1");
                assertEquals(title1, "Dune");

                book_cursor_async.moveToNext();
                String id_2 = book_cursor_async.getString(book_cursor_async.getColumnIndex("id"));
                String title2 = book_cursor_async.getString(book_cursor_async.getColumnIndex("title"));
                assertEquals(id_2, "2");
                assertEquals(title2, "1984");

                book_cursor_async.moveToNext();
                String id_3 = book_cursor_async.getString(book_cursor_async.getColumnIndex("id"));
                String title3 = book_cursor_async.getString(book_cursor_async.getColumnIndex("title"));
                assertEquals(id_3, "3");
                assertEquals(title3, "The War of the Worlds");


                book_cursor_async.moveToNext();
                String id_4 = book_cursor_async.getString(book_cursor_async.getColumnIndex("id"));
                String title4 = book_cursor_async.getString(book_cursor_async.getColumnIndex("title"));
                assertEquals(id_4, "4");
                assertEquals(title4, "Brave New World");

                book_cursor_async.moveToNext();
                String id_5 = book_cursor_async.getString(book_cursor_async.getColumnIndex("id"));
                String title5 = book_cursor_async.getString(book_cursor_async.getColumnIndex("title"));
                assertEquals(id_5, "5");
                assertEquals(title5, "Foundation");

                book_cursor_async.close();
            }

            Cursor loan_cursor_async = roomDB.query("Select * from Loan",null);
            if(loan_cursor_async.moveToFirst()){

                String idl1 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("id"));
                Long startTimel1 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("startTime"));
                Long endTimel1 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("endTime"));
                String userIdl1 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("user_id"));
                String bookIdl1 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("book_id"));

                assertEquals(idl1,"1");
                assertTrue(startTimel1 instanceof Long);
                assertTrue(endTimel1 instanceof Long);
                assertEquals(userIdl1,"1");
                assertEquals(bookIdl1,"1");

                loan_cursor_async.moveToNext();
                String idl2 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("id"));
                Long startTimel2 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("startTime"));
                Long endTimel2 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("endTime"));
                String userIdl2 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("user_id"));
                String bookIdl2 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("book_id"));

                assertEquals(idl2,"2");
                assertTrue(startTimel2 instanceof Long);
                assertTrue(endTimel2 instanceof Long);
                assertEquals(userIdl2,"2");
                assertEquals(bookIdl2,"1");

                loan_cursor_async.moveToNext();
                String idl3 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("id"));
                Long startTimel3 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("startTime"));
                Long endTimel3 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("endTime"));
                String userIdl3 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("user_id"));
                String bookIdl3 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("book_id"));

                assertEquals(idl3,"3");
                assertTrue(startTimel3 instanceof Long);
                assertTrue(endTimel3 instanceof Long);
                assertEquals(userIdl3,"2");
                assertEquals(bookIdl3,"2");

                loan_cursor_async.moveToNext();
                String idl4 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("id"));
                Long startTimel4 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("startTime"));
                Long endTimel4 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("endTime"));
                String userIdl4 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("user_id"));
                String bookIdl4 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("book_id"));

                assertEquals(idl4,"4");
                assertTrue(startTimel4 instanceof Long);
                assertTrue(endTimel4 instanceof Long);
                assertEquals(userIdl4,"2");
                assertEquals(bookIdl4,"3");

                loan_cursor_async.moveToNext();
                String idl5 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("id"));
                Long startTimel5 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("startTime"));
                Long endTimel5 = loan_cursor_async.getLong(loan_cursor_async.getColumnIndex("endTime"));
                String userIdl5 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("user_id"));
                String bookIdl5 = loan_cursor_async.getString(loan_cursor_async.getColumnIndex("book_id"));

                assertEquals(idl5,"5");
                assertTrue(startTimel5 instanceof Long);
                assertTrue(endTimel5 instanceof Long);
                assertEquals(userIdl5,"2");
                assertEquals(bookIdl5,"4");

                loan_cursor_async.close();
                //Now Test That Calling the method again won't create duplicates.

                DatabaseInitializer.populateAsync(roomDB);

                Cursor c1 = roomDB.query("Select * from User",null);
                assertEquals(c1.getCount(),3);

                c1.close();
                Cursor c2 = roomDB.query("Select * from Book",null);
                assertEquals(c2.getCount(),5);
                c2.close();
                Cursor c3 = roomDB.query("Select * from Loan",null);
                assertEquals(c3.getCount(),5);

                c3.close();

                //Reset data
                roomDB.loanModel().deleteAll();
                roomDB.bookModel().deleteAll();
                roomDB.userModel().deleteAll();

                Cursor cursor11 = roomDB.query("Select * from User",null);
                assertEquals(cursor11.getCount(),0);
                cursor11.close();

                MyViewModel viewModel = new MyViewModel(RuntimeEnvironment.application);
                viewModel.createDb();

                Cursor cursor12 = roomDB.query("Select * from User",null);
                assertEquals(cursor12.getCount(),3);
                cursor12.close();
            }
        }
    }
}
