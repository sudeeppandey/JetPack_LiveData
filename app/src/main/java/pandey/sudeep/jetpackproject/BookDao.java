package pandey.sudeep.jetpackproject;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;

@Dao
@TypeConverters(DateConverter.class)
public interface BookDao {

    @Insert(onConflict = IGNORE)
    void insertBook(Book book);

    @Query("DELETE FROM Book")
    void deleteAll();
}