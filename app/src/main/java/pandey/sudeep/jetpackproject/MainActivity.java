package pandey.sudeep.jetpackproject;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import pandey.sudeep.jetpackproject.R;


public class MainActivity extends AppCompatActivity {

    private MyViewModel mShowUserViewModel;

    private TextView mBooksTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mBooksTextView = findViewById(R.id.books_tv);

        mShowUserViewModel = ViewModelProviders.of(this).get(MyViewModel.class);

        populateDb();

        subscribeUiLoans();
    }

    private void populateDb() {

        mShowUserViewModel.createDb();
    }

    private void subscribeUiLoans() {
        mShowUserViewModel.getLoansResult().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String result) {
                mBooksTextView.setText(result);
            }
        });
    }

    public void onRefreshBtClicked(View view) {
        populateDb();
        subscribeUiLoans();
    }
}