package pandey.sudeep.jetpackproject;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSubstring;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class UIEspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void testUIInitialization(){


        onView(withSubstring("Dune")).check(matches(isDisplayed()));

        onView(withSubstring("1984")).check(matches(isDisplayed()));

        onView(withSubstring("Brave")).check(matches(isDisplayed()));

    }

    @Test
    public void buttonClickRefreshesUI(){

        onView(withId(R.id.button)).perform(click());

        onView(withSubstring("Dune")).check(matches(isDisplayed()));

        onView(withSubstring("1984")).check(matches(isDisplayed()));

        onView(withSubstring("Brave")).check(matches(isDisplayed()));

    }
}
