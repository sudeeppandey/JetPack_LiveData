package pandey.sudeep.jetpackproject;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;


import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class MvvmTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private AppDatabase appDatabase;
    private LoanDao loanDao;

    @Before
    public void initDb() throws Exception {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        appDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                AppDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();

        loanDao = appDatabase.loanModel();

    }

    @After
    public void closeDb() throws Exception {
        appDatabase.close();
    }

    @Test
    public void emptyDatabaseReturnsEmptyLiveData() throws InterruptedException {

        LiveData<List<LoanWithUserAndBook>> liveData = loanDao.findLoansByNameAfter("Mike", getYesterdayDate());
        assertEquals(LiveDataTestUtil.getValue(liveData).size(),0);

    }

    @Test
    public void livedataHoldsDataCorrectly() throws InterruptedException {

        DatabaseInitializer.populateSync(appDatabase);
        LiveData<List<LoanWithUserAndBook>> liveData = loanDao.findLoansByNameAfter("Mike", getYesterdayDate());
        assertTrue(LiveDataTestUtil.getValue(liveData).size()>0);

    }

    private Date getYesterdayDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

}
